
#ifndef BMP
#define BMP
#define HEADER_SIZE 54
#define INFO 40
#define BMP_TYPE 0x4D42
#define BIT_COUNT 24
#include "./img.h"
#include <stdio.h>
#pragma pack(push, 1)



struct bmp_header 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
#pragma pack(pop)

inline static uint8_t calculate_padding(uint32_t width);

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE = 1,
  READ_INVALID_BITS = 2,
  READ_INVALID_HEADER = 3,
  READ_ARGS_ERROR = 4,
  READ_ZERO_SIZE = 5,
  READ_INVALID_PADDING = 6
  };

static const char *strings[] = {
        [READ_OK] = "Reading was successful",
        [READ_INVALID_SIGNATURE] = "Error while getting the signature",
        [READ_INVALID_BITS] = "Error in bit size",
        [READ_INVALID_HEADER] = "Error while reading header",
        [READ_ARGS_ERROR] = "One or more of the args are NULL",
        [READ_ZERO_SIZE] = "The image was of size 0",
        [READ_INVALID_PADDING] = "The padding was invalid"
};


enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );

#endif
