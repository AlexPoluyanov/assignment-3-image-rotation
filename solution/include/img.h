#ifndef img

#include <stdint.h>
#include <stdlib.h>

    struct pixel{
        uint8_t r, g, b;
    };

    struct image {
    uint32_t width, height;
    struct pixel* data;
    };

    struct pixel* get_pixel(uint32_t x, uint32_t y, struct image const* image);

    struct image create_image(uint32_t width, uint32_t height);

    void destroy_image(struct image* img);

    uint32_t get_size(struct image const* img);

#endif
