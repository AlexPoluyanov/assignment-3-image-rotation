#include "bmp.h"
#include "../include/util.h"

inline static uint8_t calculate_padding(uint32_t width){
    return 4-(width * sizeof(struct pixel))%4;
}

enum read_status from_bmp( FILE* in, struct image* img ){
    if(in == NULL || img == NULL){
        return READ_ARGS_ERROR;
    }
    struct bmp_header bmp_header;
    if(fread(&bmp_header, HEADER_SIZE, 1, in) != 1){
        return READ_INVALID_HEADER;
    }
    *img = create_image(bmp_header.biWidth, bmp_header.biHeight);
    if(img->width == 0){
        destroy_image(img);
        return READ_ZERO_SIZE;
    }
    
    for(uint32_t y = 0; y<img->height; y++){
        for(uint32_t x = 0; x<img->width; x++){
            if(fread(get_pixel(x, y, img), sizeof(struct pixel), 1, in)!=1){
                destroy_image(img);
                return READ_INVALID_BITS;
            }
        }
        if(fseek(in, calculate_padding(img->width), SEEK_CUR) !=0){
            destroy_image(img);
            return READ_INVALID_PADDING;
        }
    }
    return READ_OK;
}

struct bmp_header create_bmp_header(struct image const* img){
    uint32_t img_size = get_size(img);
    return(struct bmp_header){
        .bfType = BMP_TYPE,
        .bfileSize = HEADER_SIZE + img_size,
        .bfReserved = 0,
        .bOffBits = HEADER_SIZE,
        .biSize = INFO,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = BIT_COUNT,
        .biCompression = 0,
        .biSizeImage = img_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    if(out==NULL || img == NULL){
        return WRITE_ERROR;
    }
    struct bmp_header header = create_bmp_header(img);
    if(fwrite(&header, HEADER_SIZE, 1, out) != 1){
        return WRITE_ERROR;
    }
    uint8_t padding = calculate_padding(img-> width);
    char zero_array[4] = {0};
    for(uint32_t y = 0; y<img->height; y++){
        if(fwrite(get_pixel(0, y, img), sizeof(struct pixel), img->width, out) != img->width){
            return WRITE_ERROR;
        }
        if(fwrite(zero_array, sizeof(char), padding, out) != padding){
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
