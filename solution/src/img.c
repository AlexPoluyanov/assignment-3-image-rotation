#include "../include/img.h"

struct pixel* get_pixel(uint32_t x, uint32_t y, struct image const* image){
    return image->data + x + y * image->width;
}

struct image create_image(uint32_t width, uint32_t height){
    struct pixel* data = malloc(width*height*sizeof(struct pixel));
    if (data != NULL){
        return (struct image) {width, height, data};
    }
    return (struct image) {0};
}

void destroy_image(struct image* img){
    if (img != 0){
        free(img->data);
        img->width=0;
        img->height=0;
        img->data=NULL;
    }
}

uint32_t get_size(struct image const* img){
    if(img!=NULL){
    return img->height*img->width;
    }
    return 0;
}
