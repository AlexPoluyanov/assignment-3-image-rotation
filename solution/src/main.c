#include "../include/bmp.h"
#include "../include/rotation.h"
#include "../include/util.h"
#include <stdio.h>

int main(int argc, char **argv) {
    if (argc != 3) {
        print_error("Wrong amount of arguments");
        return 1;
    }
    FILE *in = fopen(argv[1], "rb");
    if (in == NULL) {
        print_error("Error while opening input file");
        return 1;
    }
    struct image source = {0};
    enum read_status read_status = from_bmp(in, &source);
    if (fclose(in) == EOF) {
        print_error("Error while closing input file");
    }
    if (read_status != READ_OK) {
        print_error(strings[read_status]);
        return 1;
    }
    struct image result = rotate(source);
    destroy_image(&source);
    FILE *out = fopen(argv[2], "wb");
    if (out == NULL) {
        destroy_image(&result);
        print_error("Error while opening output file");
        return 1;
    }
    enum write_status write_status = to_bmp(out, &result);
    destroy_image(&result);
    if (fclose(out) == EOF) {
        print_error("Error while closing output file");
    }
    if (write_status == WRITE_ERROR) {
        print_error("Error while writing into output file");
        return 1;
    }
    return 0;
}
