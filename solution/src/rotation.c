#include "../include/rotation.h"
#include "../include/img.h"

struct image rotate(struct image const source) {
    struct image result = create_image(source.height, source.width);
    if(get_size(&result)>0){
    for (uint32_t y = 0; y < source.height; ++y) {
        for (uint32_t x = 0; x < source.width; ++x) {
            struct pixel* src_pixel = get_pixel(x, y, &source);
            struct pixel* dst_pixel = get_pixel(source.height - 1 - y, x, &result);
            *dst_pixel = *src_pixel;
        }
    }
    }
    return result;
}
