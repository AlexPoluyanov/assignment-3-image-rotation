#include "../include/util.h"

void print_error(const char *error){
    fprintf(stderr, "%s\n", error);
}

void print(const char *msg){
    fprintf(stdout, "%s\n", msg);
}
